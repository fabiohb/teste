import scrapy
from scrapy.loader import ItemLoader
from etl_combust_revenda.items import EtlCombustRevendaItem


class CombSpider(scrapy.Spider):
    name = "comb"
    start_urls = [
        'http://dados.gov.br/dataset/serie-historica-de-precos-de-combustiveis-por-revenda/resource/ca4ac3fc-d22c-4bfc-b914-be3a2d60de8d',
    ]

    def parse(self, response):
        relative_url = response.css('p a').attrib['href']
        absolute_url = response.urljoin(relative_url)
        loader = ItemLoader(item = EtlCombustRevendaItem())
        loader.add_value('file_urls', absolute_url)
        loader.add_value('file_name', absolute_url.split('/')[-1])
        yield loader.load_item()


class CombustiveisSpider(scrapy.Spider):
    name = "combustiveis"
    start_urls = [
        'http://dados.gov.br/dataset/serie-historica-de-precos-de-combustiveis-por-revenda',
    ]

    def parse(self, response):
        # follow links to files pages
        for a in response.css('a.heading'):
            #print(a)
            yield response.follow(a, self.parse_file)

    def parse_file(self, response):
        relative_url = response.css('p a').attrib['href']
        absolute_url = response.urljoin(relative_url)
        loader = ItemLoader(item = EtlCombustRevendaItem())
        loader.add_value('file_urls', absolute_url)
        loader.add_value('file_name', absolute_url.split('/')[-1])
        yield loader.load_item()