# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import os
from copyreg import remove_extension

from scrapy.item import Field, Item
from scrapy.loader.processors import MapCompose, TakeFirst


def remove_extension(value):
    return os.path.splitext(value)[0]


class EtlCombustRevendaItem(Item):
    file_urls = Field()
    files = Field()
    file_name = Field(
        input_processor = MapCompose(remove_extension),
        output_processor = TakeFirst()
    )