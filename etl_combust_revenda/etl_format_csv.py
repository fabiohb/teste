import csv
import os
import re

import click
from chardet.universaldetector import UniversalDetector


def format2csv(file, origem, dest, amostra=-1):
    """Formata arquivo para padrão csv."""

    file_name = '/'.join([origem, file])
    file_size = os.path.getsize(file_name)

    data = []  # create an empty list to collect the data
    progress = 0
    with click.progressbar(length=file_size, label=f'Reading {file}') as barin:
        encoding = detec_file_encoding(file_name)
        idx_line = 1
        with open(file_name, 'r', encoding=encoding) as fin:
            for line in fin:
                if (amostra > -1) and (amostra < idx_line):
                    break
                # parse_layout(line)
                line = line.strip(' ')
                line = line.strip('\t')
                if line.count('  ') > 6:
                    # substitui 2 espaços em branco por ;
                    line = line.replace('  ', ';')
                    delimiter_count = line.count(';')
                    # efeito colateral: sequencia de ; qdo arquivo tem 2 espaços e tabs
                    rx = re.search(r';{3,}', line)
                    if (rx):
                        line = line.replace(rx.group(0), '')
                elif line.count('\t') > 0:
                    # substitui TAB em branco por ;
                    line = line.replace('\t', ';')
                    delimiter_count = line.count(';')
                # tratar casos onde nome de revenda é separado em vários campos
                if delimiter_count > 10:
                    sline = line.split(';')
                    delta = delimiter_count - 10
                    tmp = []
                    tmp.extend(sline[:3])
                    tmp.extend([' '.join(sline[3:(4+delta)]).strip()])
                    tmp.extend(sline[(4+delta):])
                    line = ';'.join(tmp)
                elif delimiter_count < 9:
                    print(delimiter_count, file_name, line)
                
                if not line.count(';') in range(8,11):
                    print(line.count(';'), file_name.ljust(20), line)
                data.append(line)
                progress = progress + len(line)
                barin.update(progress)
                idx_line += 1

    file_size = len(data)
    os.makedirs(dest, exist_ok=True)
    progress = 0
    with click.progressbar(length=file_size, label=f'Writing {file}') as barout:
        with open('/'.join([dest, file]), 'w', newline='', encoding='iso-8859-1') as fout:
            fout.writelines(data)
            progress = progress + len(data)
            barout.update(progress)


def detec_file_encoding(filename, amostra=30):
    detector = UniversalDetector()
    detector.reset()
    l = 1
    with open(filename, 'rb') as file:
        for line in file:
            detector.feed(line)
            if detector.done:
                break
            l += 1
            if l > amostra:
                break
    detector.close()
    return detector.result['encoding']


def main(origem='full', dest='full-formated'):
    #files = sorted(os.listdir(origem))
    #files = ['2019-02-gasolina-etanol.csv']
    files = ['2004-1_CA.csv']
    # somente os arquivos com começam com o ano.
    rx = re.compile(r'^\d{4}')
    for file in files:
        if rx.search(file):
            format2csv(file, origem, dest)


if __name__ == "__main__":
    main()
