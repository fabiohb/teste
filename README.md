# ETL para FBI

ETL de Combustível para a disciplina de Fundamentos de Business Intelligence

## Como desenvolver?

1. Clone o repostório.
2. Crie um virtualenv com Python 3.7
3. Ative o virtualenv.
4. Instale as dependências.

```console
git clone git@gitlab.com:/fabiohb/fbi-etl.git fbi-etl
cd fbi-etl
python -m venv .venv
source .venv/bin/activate (Linux) ou .venv/scripts/activate (Windows)
pip install -r requirements.txt
scrapy crawl combustiveis
```

## Links para os documentos

https://docs.google.com/document/d/1gWa-loO8D5pDLTz-Gjw_hEDO3wJp-5Lq3ggZgcyPvh4/edit
https://docs.google.com/spreadsheets/d/173vTz_OTT_CEteG_0FK54HBt4PP3oer-Zgdw6kfwrUw/edit

